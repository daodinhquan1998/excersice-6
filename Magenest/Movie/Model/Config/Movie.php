<?php

namespace Magenest\Movie\Model\Config;


/**
 * Class Status
 * @package ViMagento\HelloWorld\Model\Config
 */
class Movie implements \Magento\Framework\Option\ArrayInterface
{
    protected $movieFactory;

    public function __construct(\Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory $movieFactory)
    {
        $this->movieFactory = $movieFactory;
    }

    /**
     * @return array[]
     */
    public function toOptionArray()
    {
        $data = $this->movieFactory->create();
        $option = [];
        foreach ($data as $item)
        {
            $option[] = ['value' => $item->getId(), 'label' => $item->getName()];
        }
        return $option;
    }
}