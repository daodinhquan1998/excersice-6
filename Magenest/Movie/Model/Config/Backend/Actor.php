<?php

namespace Magenest\Movie\Model\Config\Backend;
use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory;


class Actor extends \Magento\Framework\App\Config\Value
{
    protected $collection;
    public function __construct(\Magento\Framework\Model\Context $context,
                                \Magento\Framework\Registry $registry,
                                \Magento\Framework\App\Config\ScopeConfigInterface $config,
                                CollectionFactory $collectionFactory,
                                \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
                                \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
                                \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
                                array $data = [])
    {
        $this->collection = $collectionFactory->create();
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function beforeSave()
    {
        $count = $this->collection->getSize();

        $this->setValue(intval($count));
        parent::beforeSave();
    }
}