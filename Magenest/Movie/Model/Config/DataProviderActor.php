<?php

namespace Magenest\Movie\Model\Config;

use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory;
use Magenest\Movie\Model\MovieActorFactory;

class DataProviderActor extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $actorCollection;
    protected $movieActorFactory;
    protected $_loadedData;
    protected $collection;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        MovieActorFactory $movieActorFactory,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->actorCollection = $collectionFactory;
        $this->movieActorFactory = $movieActorFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->_loadedData[$item->getId()] = $item->getData();
            $nameMovie = $this->movieActorFactory->create()->load($item->getId(),'actor_id')->getData('movie_id');
            $this->_loadedData[$item->getId()]['movie_id'] = $nameMovie;
        }
        return $this->_loadedData;
    }

}