<?php

namespace Magenest\Movie\Model\Config;


/**
 * Class Status
 * @package ViMagento\HelloWorld\Model\Config
 */
class Director implements \Magento\Framework\Option\ArrayInterface
{
    protected $directorFactory;

    public function __construct(\Magenest\Movie\Model\ResourceModel\Director\CollectionFactory $directorFactory)
    {
        $this->directorFactory = $directorFactory;
    }

    /**
     * @return array[]
     */
    public function toOptionArray()
    {
        $data = $this->directorFactory->create();
        $option = [];
        foreach ($data as $item)
        {
            $option[] = ['value' => $item->getId(), 'label' => $item->getName()];
        }
        return $option;
    }
}