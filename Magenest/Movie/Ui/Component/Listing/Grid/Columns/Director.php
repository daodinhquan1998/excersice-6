<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Movie\Ui\Component\Listing\Grid\Columns;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Review\Helper\Data as StatusSource;
use Magenest\Movie\Model\DirectorFactory;
/**
 * Class Status
 *
 * @api
 * @since 100.1.0
 */
class Director extends Column implements OptionSourceInterface
{
    /**
     * @var StatusSource
     * @since 100.1.0
     */
    protected $source;
    protected $directorFactory;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StatusSource $source
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StatusSource $source,
        DirectorFactory $directorFactory,
        array $components = [],
        array $data = []
    ) {
        $this->directorFactory = $directorFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->source = $source;
    }

    /**
     * {@inheritdoc}
     * @since 100.1.0
     */
    public function prepareDataSource(array $dataSource)
    {
        $dataSource = parent::prepareDataSource($dataSource);
        if (empty($dataSource['data']['items'])) {
            return $dataSource;
        }
        // $datasource['data']['items'] : item la cac cot , data la du lieu cua cot
        // option->load($item['director_id'] su dug de load 1 model ,lay ra 1 object
        // 1 object thi se co cac magic function nhu getdata, getName,getID
        foreach ($dataSource['data']['items'] as &$item) {
            $options = $this->directorFactory->create();
            //create 1 model
            if (isset($item['director_id'])) {
                $item['director_id'] = $options->load($item['director_id'])->getName();
            }
        }
        return $dataSource;
    }

    /**
     * {@inheritdoc}
     * @since 100.1.0
     */
    public function toOptionArray()
    {
        return $this->source->getReviewStatusesOptionArray();
    }
}
