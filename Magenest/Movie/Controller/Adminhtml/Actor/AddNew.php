<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class AddNew extends \Magento\Backend\App\Action
{
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Actor'));
        return $resultPage;
    }
}