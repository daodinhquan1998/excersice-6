<?php


namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Framework\View\Result\PageFactory;
use \Magento\Backend\App\Action;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectoryCollection;


class ListActor extends \Magento\Backend\App\Action
{
    protected $_pageFactory;

    public function __construct(Action\Context $context, PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Actor Listing'));
        return $resultPage;
    }
}