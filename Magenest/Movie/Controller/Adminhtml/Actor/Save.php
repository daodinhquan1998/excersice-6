<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magenest\Movie\Model\ActorFactory;
use Magento\Backend\App\Action;
use Magenest\Movie\Model\MovieActorFactory;


/**
 * Class Save
 * @package ViMagento\HelloWorld\Controller\Adminhtml\Post
 */
class Save extends Action
{
    /**
     * @var PostFactory
     */
    private $actorFactory;
    private $movieActorFactory;
    /**
     * Save constructor.
     * @param Action\Context $context
     * @param PostFactory $movieFactory
     */
    public function __construct(
        Action\Context $context,
        ActorFactory $actorFactory,
        MovieActorFactory $movieActorFactory
    ) {
        parent::__construct($context);
        $this->actorFactory = $actorFactory;
        $this->movieActorFactory = $movieActorFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['actor_id']) ? $data['actor_id'] : null;

        $newData = [
            'name' => $data['name']
        ];

        $actor = $this->actorFactory->create();
        $movieActor = $this->movieActorFactory->create();
        if ($id) {
            $actor->load($id);
            $movieActor->load($id,'actor_id')->setData('movie_id',$data['movie_id'])->save();
            // movieActor load xem trong db co id la actor_id khong.
            // neu co setData cho truong movie_id(o trong form) = data['movie_id]->luu lai
            $this->getMessageManager()->addSuccessMessage(__('Edit '));
        }
        try {
            $actor->addData($newData);
            //   $this->_eventManager->dispatch("vimagento_post_before_save", ['postData' => $movie]);
            $actor->save();

            $newMovieActor = [
                'actor_id' => $actor->getId(),
                'movie_id' => $data['movie_id']
            ];

            $movieActor->addData($newMovieActor);
            $movieActor->save();
            $this->messageManager->addSuccessMessage(__('You saved the post.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        return $this->resultRedirectFactory->create()->setPath('admin/actor/listactor');
    }
}