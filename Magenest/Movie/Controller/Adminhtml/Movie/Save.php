<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magenest\Movie\Model\MovieFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package ViMagento\HelloWorld\Controller\Adminhtml\Post
 */
class Save extends Action
{
    /**
     * @var PostFactory
     */
    private $movieFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param PostFactory $movieFactory
     */
    public function __construct(
        Action\Context $context,
        MovieFactory $movieFactory
    ) {
        parent::__construct($context);
        $this->movieFactory = $movieFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['movie_id']) ? $data['movie_id'] : null;

        $newData = [
            'name' => $data['name'],
            'rating' => $data['rating'],
            'description' => $data['description'],
            'director_id' => $data['director_id']
        ];

        $movie = $this->movieFactory->create();

        if ($id) {
            $movie->load($id);
            $this->getMessageManager()->addSuccessMessage(__('Edit '));
        }
        try {
            $movie->addData($newData);
            //   $this->_eventManager->dispatch("vimagento_post_before_save", ['postData' => $movie]);
            $movie->save();
            $this->messageManager->addSuccessMessage(__('You saved the post.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('admin/movie/listmovie');
    }
}