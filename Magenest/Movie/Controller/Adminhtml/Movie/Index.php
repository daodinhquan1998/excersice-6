<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Framework\View\Result\PageFactory;
use \Magento\Backend\App\Action;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectoryCollection;
use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory as ActorCollection;
use Magenest\Movie\Model\ResourceModel\MovieActor\CollectionFactory as MovieActorCollection;


class Index extends \Magento\Backend\App\Action
{
    protected $_pageFactory;
    protected $movieFactory;
    protected $directorFactory;
    protected $actorFactory;
    protected $movieActorFactory;

    public function __construct(Action\Context       $context,
                                PageFactory          $pageFactory,
                                CollectionFactory    $movieFactory,
                                DirectoryCollection  $directorFactory,
                                ActorCollection      $actorFactory,
                                MovieActorCollection $movieActorFactory)
    {
        $this->_pageFactory = $pageFactory;
        $this->movieFactory = $movieFactory;
        $this->directorFactory = $directorFactory;
        $this->actorFactory = $actorFactory;
        $this->movieActorFactory = $movieActorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->movieFactory->create();
        $collection->getSelect()->joinLeft(
            ['md' => $collection->getTable('magenest_director')],
            "main_table.director_id = md.director_id",
            [
                'director_name' => 'md.name'
            ])->joinLeft(
                ['mma' => $collection->getTable('magenest_movie_actor')],
                "main_table.movie_id = mma.movie_id",
                ['actor_id' => 'mma.actor_id']
            );

        $temp = [];
        foreach ($collection->getData() as $item){ // tai sao lai dung mang ->getData() o day ??
            if(!isset($temp[$item['movie_id']])) {
                $temp[$item['movie_id']] = $item;
                // $temp[4] = item
            }
            echo "<pre>";
            $name = $this->actorFactory->create()->addFieldToFilter('actor_id',['eq'=> $item['actor_id']]);
            foreach ($name->getItems() as $value)
            {
                $temp[$item['movie_id']][] = $value->getName();
            }
        }
        print_r($temp);

//        foreach ($collection->getData() as $item){
//            echo "<pre>";
//            $name = $this->actorFactory->create()->addFieldToFilter('actor_id',['eq'=> $item['actor_id']]);
//            foreach ($name->getItems() as $value)
//            {
//                $item[] = $value->getName();
//            }
//            print_r($item);
//        }

//            print_r($collection->getData());
//
//

//        foreach ($movie as $value) {
//            echo "<pre>";
//            $director = $this->directorFactory->create()->addFieldToSelect('name')->addFieldToFilter('director_id',$value['director_id']);
//            print_r($value->getData());
//            print_r($director->getData());
//            echo "</pre>";
//        }


//        $resultPage = $this->_pageFactory->create();
//        $resultPage->getConfig()->getTitle()->prepend(__('Magenest Listing'));
//        return $resultPage;
    }
}