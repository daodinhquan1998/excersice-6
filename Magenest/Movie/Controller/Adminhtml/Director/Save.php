<?php

namespace Magenest\Movie\Controller\Adminhtml\Director;

use Magenest\Movie\Model\DirectorFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package ViMagento\HelloWorld\Controller\Adminhtml\Post
 */
class Save extends Action
{
    /**
     * @var DirectorFactory
     */
    private $directorFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param DirectorFactory $directorFactory
     */
    public function __construct(
        Action\Context $context,
        DirectorFactory $directorFactory
    ) {
        parent::__construct($context);
        $this->directorFactory = $directorFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['director_id']) ? $data['director_id'] : null;

        $newData = [
            'name' => $data['name'],
        ];

        $director = $this->directorFactory->create();

        if ($id) {
            $director->load($id);
            $this->getMessageManager()->addSuccessMessage(__('Edit '));
        }
        try {
            $director->addData($newData);
            //   $this->_eventManager->dispatch("vimagento_post_before_save", ['postData' => $director]);
            $director->save();
            $this->messageManager->addSuccessMessage(__('You saved the post.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('admin/director/index');
    }
}