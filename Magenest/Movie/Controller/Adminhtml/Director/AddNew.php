<?php

namespace Magenest\Movie\Controller\Adminhtml\Director;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;


class AddNew extends Action
{

    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Director'));
        return $resultPage;
    }
}