<?php

namespace Magenest\Movie\Block\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;
class Button extends Field
{
//    protected $_template = 'Magenest_Movie::system/config/fieldset/button.phtml';

    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout(); // TODO: Change the autogenerated stub
        if(!$this->getTemplate()) {
            $this->setTemplate('system/config/fieldset/button.phtml');
        }
        return $this;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
       return $this->_toHtml();
    }
}

